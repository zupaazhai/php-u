<?php

$utils = array(
    'simple_html_dom/simple_html_dom.php',
);

foreach ( $utils as $u ) {
    require_once 'utils/' . $u;
}
